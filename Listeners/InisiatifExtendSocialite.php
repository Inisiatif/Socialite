<?php declare(strict_types=1);

namespace Inisiatif\Component\Socialite\Listeners;

use SocialiteProviders\Manager\SocialiteWasCalled;
use Inisiatif\Component\Socialite\Providers\InisiatifProvider;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
final class InisiatifExtendSocialite
{
    /**
     * @param SocialiteWasCalled $socialite
     */
    public function handle(SocialiteWasCalled $socialite): void
    {
        $socialite->extendSocialite('inisiatif', InisiatifProvider::class);
    }
}
